import { getVersionedLocalStorage, VersionedLocalStorage } from './versioned-local-storage';

describe('versioned local storage', () => {

	afterEach(() => localStorage.removeItem('storageKey'));

	it('should not update localStorage before build', () => {
		getVersionedLocalStorage('storageKey')
			.addVersion(() => 'some string');
		expect(localStorage.getItem('storageKey')).toBeNull();
	});

	it('should update localStorage on build', () => {
		getVersionedLocalStorage('storageKey')
			.addVersion(() => 'some string')
			.build();
		expect(localStorage.getItem('storageKey')).not.toBeNull();
	});

	it('should empty the localStorage', () => {
		getVersionedLocalStorage('storageKey')
			.addVersion(() => 'some string')
			.build();
		getVersionedLocalStorage('storageKey')
			.addVersion(() => 'some string')
			.deprecate();
		expect(localStorage.getItem('storageKey')).toBeNull();
	});

	it('should update the value to a new version', () => {
		getVersionedLocalStorage('storageKey')
			.addVersion(() => 'some string')
			.build();

		const versionedLocalStorage = getVersionedLocalStorage('storageKey')
			.addVersion(() => 'some string')
			.addVersion(oldString => ({ stringValue: oldString }))
			.build();

		expect(versionedLocalStorage.value).toEqual({
			stringValue: 'some string',
		});
	});

	describe('with storage', () => {
		let versionedLocalStorage: VersionedLocalStorage<{ stringValue?: string }>;

		beforeEach(() =>
			versionedLocalStorage = getVersionedLocalStorage('storageKey')
				.addVersion(() => 'some string')
				.addVersion(oldString => ({ stringValue: oldString }))
				.addVersion<{ stringValue?: string }>(value => value)
				.build());

		it('should handle overwriting the storage instance', () => {
			expect(versionedLocalStorage.value).not.toBeNull();
			versionedLocalStorage = getVersionedLocalStorage('storageKey')
				.addVersion(() => 'some string')
				.addVersion(oldString => ({ stringValue: oldString }))
				.addVersion<{ stringValue?: string }>(value => value)
				.build();

			expect(versionedLocalStorage.value).not.toBeNull();
		});

		it('should get the storage key', () => {
			expect(versionedLocalStorage.storageKey).toBe('storageKey');
		});

		it('should get read the version number', () => {
			expect(versionedLocalStorage.version).toBe(3);
		});

		it('should read the value', () => {
			expect(versionedLocalStorage.value)
				.toEqual({ stringValue: 'some string' });
		});

		it('should update the value', () => {
			versionedLocalStorage.value = {};
			expect(versionedLocalStorage.value)
				.toEqual({});
		});
	});
});
