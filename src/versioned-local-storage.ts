type StoredValue<TValue> = {
	version: number,
	value: TValue,
}

type Transformer = {
	sourceVersion: number;
	transform: (previousValue: any) => any;
};

class VersionedLocalStorageBase<TValue> {
	constructor(
		readonly version: number,
		readonly storageKey: string,
	) {
	}

	protected getStoredValue(): StoredValue<TValue> | null {
		const item = localStorage.getItem(this.storageKey);
		if (item === null)
			return null;
		return JSON.parse(item) as StoredValue<TValue>;
	}

	protected setStoredValue<TValue>(newValue: TValue) {
		localStorage.setItem(
			this.storageKey,
			JSON.stringify({
				version: this.version,
				value: newValue,
			}),
		);
	}
}

class VersionedLocalStorageBuilder<TValue> extends VersionedLocalStorageBase<TValue> {
	constructor(
		storageKey: string,
		version: number = 0,
		private readonly transformers: Transformer[] = [],
	) {
		super(version, storageKey);
	}

	build() {
		this.setStoredValue(this.getMigratedValue());

		return new VersionedLocalStorageConstructor<TValue>(
			this.version,
			this.storageKey,
		);
	}

	deprecate(): void {
		localStorage.removeItem(this.storageKey);
	}

	addVersion<TNewValue>(
		transformer: (oldValue: TValue) => TNewValue,
	): VersionedLocalStorageBuilder<TNewValue> {
		return new VersionedLocalStorageBuilder<TNewValue>(
			this.storageKey,
			this.version + 1,
			[
				...this.transformers,
				{
					sourceVersion: this.version,
					transform: transformer,
				},
			],
		);
	}

	private getMigratedValue() {
		const currentVersion = this.getCurrentVersion();

		const transformers = this.transformers
			.filter(transformer => transformer.sourceVersion >= currentVersion);

		const currentValue = this.getStoredValue()?.value ?? null;

		return transformers
			.reduce<any>(
				(value, transformer) => transformer.transform(value),
				currentValue,
			);
	}

	private getCurrentVersion() {
		return this.getStoredValue()?.version ?? 0;
	}
}

export class VersionedLocalStorage<TValue> extends VersionedLocalStorageBase<TValue> {
	get value() {
		const storedValue = this.getStoredValue();

		if (storedValue === null)
			throw new Error('localStorage state is invalid');

		return storedValue.value;
	}

	set value(newValue: TValue) {
		this.setStoredValue(newValue);
	}
}

const VersionedLocalStorageConstructor =
	VersionedLocalStorage as {
		new<TValue>(
			version: number,
			storageKey: string,
		): VersionedLocalStorage<TValue>
	};

export function getVersionedLocalStorage(storageKey: string): VersionedLocalStorageBuilder<null> {
	return new VersionedLocalStorageBuilder<null>(storageKey);
}
